# 12travel - advanced travel search system


![photo6043989712961645505](/uploads/d45a120b4fc0a3a95341f065f5b58453/photo6043989712961645505.jpg)

## **Vision**
12travel è una piattaforma che vuole cambiare il modo in cui viaggiamo.
Un motore di ricerca che combina varie tipologie di trasporto ed una serie di servizi extra per personalizzare e rendere unico ogni viaggio. Ad aumentare l'appeal della web app la possibilità di prenotare e pagare le varie **combinazioni** di viaggio proposte in un'unica soluzione di pacchetto.

##### Mezzi di trasporto:
- autobus
- aerei
- treni
- car sharing

##### Servizi extra:
- hotel
- noleggio auto
- esperienze (vedi airBnb)

##### Obiettivi:
- trovare il modo più economico per spostarsi da un punto A ad un punto B.
- diventare un punto cardine dell'organizzazione e della ricerca dei viaggi (Skyscanner, Drungli, eDreams, Flixbus)

## **Wireframe draft**

![photo6043989712961645533](/uploads/557f23fd993717ff678ba086fb9b9d5c/photo6043989712961645533.jpg)

## **User story**
Come utente generico che visita la piattaforma posso personalizzare le modalità di ricerca del mio viaggio ideale:

##### Tipo Ricerca 1: A->B
Seleziono città di partenza (A) e destinazione (B), data e numero di passeggeri.
##### Tipo Ricerca 2: A->X->B
Seleziono città di partenza (A) di destinazione (B) e tappa intermedia (X), data e numero di passeggeri.
##### Tipo Ricerca 3: A->X1->..->B
Seleziono città di partenza (A) di destinazione (B) e le tappe intermedie (X1,X2...), data e numero di passeggeri.

![Mockup_agenziaviaggi](/uploads/bdf037e7c1089da2ab7a168de0a89563/Mockup_agenziaviaggi.PNG)

##### Servizi extra
Tramite una checkbox ho la possibilità di aggiungere dei servizi extra alla mia ricerca (hotel, noleggio auto, esperienze, ecc).
##### Search
Il servizio mi propone in ordine di prezzo crescente le soluzioni del mio viaggio.
##### Select
Selezione del pacchetto proposto:
- solo viaggi
- viaggi + extra

##### Data & Checkout
Inserimento dati viaggiatori e pagamento

## **Sketch**

![photo6043989712961645529](/uploads/010337a875e84daa6d2247b072321cbd/photo6043989712961645529.jpg)
* il db nello sketch è primario ma in realtà viene usato come supporto nel progetto

## **Tabella attori-obiettivi**
#### Pattern Decorator:

![diagram-decorator](/uploads/0f652351147b8f3f7e8718341e8291fd/diagram-decorator.PNG)

#### Pattern State:

![diagram-state2](/uploads/d8a7fc161e527e1797b8ce28cabc5742/diagram-state2.PNG)

## **Struttura della cartella**



## **Stretch Goals**
- Modificare la prenotazione
- Aggiunger login e logout
- Diventare virali
- Passare l'esame





