package prenotazioni;

public class PrenotazioneAnnullata extends StatoPrenotazione {

    public PrenotazioneAnnullata(Prenotazione p) {
        super(p);
    }

    @Override
    public void insert() {
        System.out.println("PRENOTAZIONE ANNULLATA");
    }

    @Override
    public void submit() {
        System.out.println("PRENOTAZIONE ANNULLATA");    
    }

    @Override
    public void cancel() {
        System.out.println("PRENOTAZIONE ANNULLATA");
    }
    
    @Override
    public void print() {
        System.out.println("Stato prenotazione: ANNULLATA");
    }
    
}
