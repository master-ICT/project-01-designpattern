package prenotazioni;

public class PrenotazioneConfermata extends StatoPrenotazione {

    public PrenotazioneConfermata(Prenotazione p) {
        super(p);
    }

    @Override
    public void insert() {
        System.out.println("Inserisco i dati di pagamento");
    }

    @Override
    public void submit() {
        System.out.println("Submit dati pagamento");
        p.setState(new PrenotazionePagata(p));
    }

    @Override
    public void cancel() {
        System.out.println("ANNULLA");   
        p.setState(new PrenotazioneAnnullata(p));
    }
    
    @Override
    public void print() {
        System.out.println("Stato prenotazione: CONFERMATA");
    }
}
