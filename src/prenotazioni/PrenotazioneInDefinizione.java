package prenotazioni;

public class PrenotazioneInDefinizione extends StatoPrenotazione {
    
    public PrenotazioneInDefinizione(Prenotazione p) {
        super(p);
    }

    @Override
    public void insert() {
        System.out.println("Inserisco i dati delle tappe");
    }

    @Override
    public void submit() {
        System.out.println("Submit dati tappe");
        p.setState(new PrenotazioneConfermata(p));
    }

    @Override
    public void cancel() {
        System.out.println("ANNULLA");   
        p.setState(new PrenotazioneAnnullata(p));
    }  

    @Override
    public void print() {
        System.out.println("Stato prenotazione: IN DEFINIZIONE");
    }
}
