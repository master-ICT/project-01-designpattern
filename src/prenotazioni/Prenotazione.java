package prenotazioni;

import viaggi.Viaggio;

public class Prenotazione {
    
    protected StatoPrenotazione sp;
    protected Viaggio v;
    
    public Prenotazione(String partenza) {
        this.sp = new PrenotazioneInDefinizione(this);
        this.v = new Viaggio(partenza);
    }
    
    public void setState(StatoPrenotazione nuovo) {
        this.sp = nuovo;
    }

    public Viaggio getViaggio() {
        return v;
    }
    
    public void insert() {
        sp.insert();
    }

    public void submit() {
        sp.submit();
    }

    public void cancel() {
        sp.cancel();
    }  
    
    public void print() {
        sp.print();
    } 
}
