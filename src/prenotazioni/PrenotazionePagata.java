package prenotazioni;

public class PrenotazionePagata extends StatoPrenotazione {

    String dati;
    
    public PrenotazionePagata(Prenotazione p) {
        super(p);
    }
    
    @Override
    public void insert() {
        System.out.println("Dati pagamento già inseriti");
    }

    @Override
    public void submit() {
        System.out.println("Dati pagamento già inseriti");
    }

    @Override
    public void cancel() {
        System.out.println("ANNULLA");   
        p.setState(new PrenotazioneAnnullata(p));
    }
    
    @Override
    public void print() {
        System.out.println("Stato prenotazione: PAGATA");
    }
}
