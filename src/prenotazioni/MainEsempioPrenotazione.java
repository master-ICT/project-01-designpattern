package prenotazioni;

import viaggi.*;

public class MainEsempioPrenotazione {
    
    public static void main(String[] args) {
         
        Prenotazione p = new Prenotazione("Torino");
        
		Tappa t = new TappaBasic("Dubai");
		t = new TappaConNoleggio(t, "Hyundai i20");
		t = new TappaConHotel(t, "Hilton Luxury");
        Tappa t2 = new TappaBasic("Cagliari");
        Tappa t3 = new TappaConHotel(new TappaBasic("Frosinone"), "Hotel Principi");
        
        p.getViaggio().aggiungiTappa(t);
        p.getViaggio().aggiungiTappa(t2);
        p.getViaggio().aggiungiTappa(t3);
        
        p.getViaggio().riepilogo();
        
        p.insert();
        p.print();
        p.submit();
        p.print();

        // Qui l'utente metterebbe i dati di pagamento
        p.insert();
        p.print();
        p.submit();
        p.print();
        
        p.cancel(); //Annullo
        p.print();
        // Qui non posso più fare nulla
        p.insert(); 
        p.insert();
    }
}
