package prenotazioni;

public abstract class StatoPrenotazione {
    
    protected Prenotazione p;
    
    public StatoPrenotazione(Prenotazione p) {
        this.p = p;
    }
    
    public abstract void insert();
    public abstract void submit();
    public abstract void cancel();
    public abstract void print();
}
