package viaggi;

public class MainEsempioViaggio {

	public static void main(String[] args) {

		Viaggio viagg = new Viaggio("Torino");

		Tappa t = new TappaBasic("Dubai");
		t.doveSono();

		t = new TappaConNoleggio(t, "Hyundai i20");
		t.doveSono();

		t = new TappaConHotel(t, "Hilton Luxury");
		t.doveSono();

        Tappa t2 = new TappaBasic("Cagliari");
        
        Tappa t3 = new TappaConHotel(new TappaBasic("Frosinone"), "Hotel Principi");
        
        System.out.println("\n");
        
        viagg.aggiungiTappa(t);
        viagg.aggiungiTappa(t2);
        viagg.aggiungiTappa(t3);
        
        viagg.riepilogo();
	}
}