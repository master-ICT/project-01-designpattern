package viaggi;

public class TappaConHotel extends TappaDecorator {

	String hotel;

	public TappaConHotel(Tappa t, String h) {
		super(t);
		this.hotel = h;
	}

	public void doveDormo() {
		System.out.println("Dormo all'hotel " + hotel);
	}

	public void doveSono() {
		super.doveSono();
		doveDormo();
	}
}