package viaggi;

public class TappaBasic implements Tappa {

	String luogo;

	public TappaBasic(String l) {
		this.luogo = l;
	}

	public void doveSono() {
		System.out.println("Sono a " + luogo);
	}
}
