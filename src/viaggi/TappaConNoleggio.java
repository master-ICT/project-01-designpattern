package viaggi;

public class TappaConNoleggio extends TappaDecorator {

	String macchina;

	public TappaConNoleggio(Tappa t, String m) {
		super(t);
		this.macchina = m;
	}

	public void cheMacchina() {
		System.out.println("Ho la macchina " + macchina);
	}

	public void doveSono() {
		super.doveSono();
		cheMacchina();
	}
}