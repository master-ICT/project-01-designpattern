package viaggi;

public abstract class TappaDecorator implements Tappa {

	protected Tappa t;

	public TappaDecorator(Tappa t) {
		this.t = t;
	}

	public void doveSono() {
		t.doveSono();
	}
}