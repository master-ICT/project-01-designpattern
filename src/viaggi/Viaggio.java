package viaggi;

import java.util.*;

public class Viaggio {

	private String partenzaArrivo;
	private List<Tappa> tappe;

	public Viaggio(String pa) {
		this.partenzaArrivo = pa;
		this.tappe = new LinkedList<>();
	}

	public void aggiungiTappa(Tappa t) {
		tappe.add(t);
	}

	public void togliTappa(Tappa t) {
		tappe.remove(t);
	}
    
    public void riepilogo() {
        System.out.println("Partenza da " + partenzaArrivo + "\n");
        for (int i=0; i<tappe.size(); i++) {
            System.out.println("Tappa " + (i+1) + ":");
            tappe.get(i).doveSono();
            System.out.println("\n");
        }
    }
}